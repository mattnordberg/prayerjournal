import { Component } from '@angular/core';

import { Platform, NavController, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import { Network } from '@ionic-native/network/ngx';
import { AngularFirestore } from '@angular/fire/firestore';
import { ThemeService } from './services/theme/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  profile: any;

  constructor(
    private auth: AngularFireAuth,
    private menu: MenuController,
    private nav: NavController,
    private network: Network,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private store: AngularFirestore,
    private themeService: ThemeService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.getUser();
    this.hideSplashScreen();
    this.watchAuthState();
    this.watchNetworkChanges();
  }

  closeMenu() {
    this.menu.close();
  }

  getUser() {
    this.auth.user.subscribe(user => {
      if (!user) {
        this.themeService.setTheme('');
        return;
      }
      const profileDoc = this.store.collection('user-profiles').doc(user.uid);
      profileDoc.valueChanges().subscribe((profile: any) => {
        this.profile = profile;
      });
      this.store.collection('user-settings').doc(user.uid).valueChanges().subscribe((settings: any) => {
        this.themeService.setTheme(settings.theme);
      });
    });
  }

  hideSplashScreen() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  watchAuthState() {
    this.auth.authState.subscribe((user) => {
      if (user) {
        this.nav.navigateRoot('/tabs');
        this.menu.enable(true);
      } else {
        this.nav.navigateRoot('/start');
        this.menu.enable(false);
      }
    });
  }

  watchNetworkChanges() {
    this.auth.user.subscribe(user => {
      if (user) {
        this.store.collection('user-settings').doc(user.uid).valueChanges().subscribe((settings: any) => {
          if (settings) {
            this.network.onConnect().subscribe(() => {
              debugger;
              if (this.network.type != this.network.Connection.WIFI && settings.wifiOnly) {
                this.store.firestore.disableNetwork();
              } else if (!settings.offlineMode) {
                this.store.firestore.enableNetwork();
              }
            });
          } else {
            this.store.firestore.enableNetwork();
          }
        });
      } else {
        this.store.firestore.enableNetwork();
      }
    });
  }
}
