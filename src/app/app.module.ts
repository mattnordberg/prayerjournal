import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRouting } from './app.routing';
import { AppComponent } from './app.component';
import { PrayersService } from './services/prayers/prayers.service';
import { IonicStorageModule } from '@ionic/storage';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { Network } from '@ionic-native/network/ngx';
import { RequestsService } from './services/requests/requests.service';
import { PopoversModule } from './popovers/popovers.module';
import { ComponentsModule } from './components/components.module';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

// This switch should control dev mode for the whole app
const isDevMode = true;

const devConfig = {
  apiKey: 'AIzaSyBbjtcMS7hWD4aR_YfN_QQnx0Mw44QKsOA',
  authDomain: 'prayer-journal-mnordberg-dev.firebaseapp.com',
  databaseURL: 'https://prayer-journal-mnordberg-dev.firebaseio.com',
  projectId: 'prayer-journal-mnordberg-dev',
  storageBucket: 'prayer-journal-mnordberg-dev.appspot.com',
  messagingSenderId: '1074326999904'
};

export const authDomain = isDevMode ? devConfig.authDomain : '';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    AngularFireModule.initializeApp(isDevMode ? devConfig : {}),
    AngularFireAuthModule,
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule,
    AppRouting,
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    NgCircleProgressModule.forRoot({
      radius: 100,
      outerStrokeWidth: 8,
      showInnerStroke: false,
      outerStrokeColor: '#3880ff',
      animationDuration: 300,
      backgroundOpacity: 0,
      showTitle: false,
      showUnits: false,
      showSubtitle: false,
      outerStrokeLinecap: 'butt',
      responsive: true
    }),
    PopoversModule,
    ComponentsModule
  ],
  providers: [
    GooglePlus,
    Network,
    PrayersService,
    RequestsService,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
