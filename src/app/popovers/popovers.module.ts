import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../components/components.module';
import { GroupedListPopover } from './grouped-list/grouped-list.popover';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    ComponentsModule
  ],
  declarations: [GroupedListPopover],
  entryComponents: [GroupedListPopover],
  exports: [GroupedListPopover]
})
export class PopoversModule {}
