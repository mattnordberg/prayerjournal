import { Component, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ListItem } from 'src/app/services/common/list-item.model';

@Component({
  selector: 'app-grouped-list-popover',
  templateUrl: 'grouped-list.popover.html',
  styleUrls: ['grouped-list.popover.scss']
})
export class GroupedListPopover {

  @Input() title: string;

  @Input() items: ListItem[] = [];

  constructor(
    private popoverCtrl: PopoverController
  ) {
  }

  select(item: ListItem) {
    this.popoverCtrl.dismiss(item);
  }

  cancel() {
    this.popoverCtrl.dismiss();
  }

}
