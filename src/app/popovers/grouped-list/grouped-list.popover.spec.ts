import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupedListPopover } from './grouped-list.popover';

describe('GroupedListPopover', () => {
  let component: GroupedListPopover;
  let fixture: ComponentFixture<GroupedListPopover>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GroupedListPopover],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedListPopover);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
