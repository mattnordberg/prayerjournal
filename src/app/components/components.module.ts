import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupedListComponent } from './grouped-list/grouped-list.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule
  ],
  declarations: [GroupedListComponent],
  exports: [GroupedListComponent]
})
export class ComponentsModule { }
