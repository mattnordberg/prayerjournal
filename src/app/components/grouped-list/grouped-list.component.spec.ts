import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupedListComponent } from './grouped-list.component';

describe('GroupedListComponent', () => {
  let component: GroupedListComponent;
  let fixture: ComponentFixture<GroupedListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GroupedListComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
