import { Component, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { ItemGroup } from 'src/app/services/common/item-group.model';
import { ListItem } from 'src/app/services/common/list-item.model';
import { IonItemSliding } from '@ionic/angular';

@Component({
  selector: 'app-grouped-list',
  templateUrl: 'grouped-list.component.html',
  styleUrls: ['grouped-list.component.scss']
})
export class GroupedListComponent implements OnChanges {

  @Input() items: ListItem[] = [];
  @Input() previewLength: number;
  @Input() groupBy: string;
  @Input() ready: boolean;

  @Input() canSwipeStart: Function;
  @Input() swipeStartColor: string;
  @Input() swipeStartIcon: string;

  @Input() canSwipeEnd: Function;
  @Input() swipeEndColor: string;
  @Input() swipeEndIcon: string;

  @Output() select: EventEmitter<ListItem> = new EventEmitter<ListItem>();
  @Output() swipeStart: EventEmitter<ListItem> = new EventEmitter<ListItem>();
  @Output() swipeEnd: EventEmitter<ListItem> = new EventEmitter<ListItem>();

  groups: ItemGroup<ListItem>[] = [];

  constructor() {

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.items && this.items) {
      this.groups = [];
      this.items.forEach(item => {
        const words = (item.text || '').split(' ');
        const maxWords = this.previewLength || 10;
        item.preview = words.length > maxWords ? (words.slice(0, maxWords).join(' ') + '...') : words.join(' ');
        const groupBy = this.groupBy || 'date';
        let group = this.groups.find(g => g.label === item[groupBy]);
        if (!group) {
          group = new ItemGroup<ListItem>();
          group.label = item[groupBy];
          this.groups.push(group);
        }
        group.items.push(item);
      });
    }
  }

  onClick(item: ListItem) {
    this.select.emit(item);
  }

  onSwipe(item: ListItem, slider: IonItemSliding, side: string) {
    slider.close();
    if (side === 'start') {
      this.swipeStart.emit(item);
    }
    if (side === 'end') {
      this.swipeEnd.emit(item);
    }
  }

}
