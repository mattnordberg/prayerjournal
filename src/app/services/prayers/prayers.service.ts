import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Prayer } from './prayer.model';
import {  map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import * as moment from 'moment';
import { ItemGroup } from '../common/item-group.model';
import { Observable } from 'rxjs';
import { FirestoreRepositoryService } from '../common/firestore-repository.service';

@Injectable({
  providedIn: 'root'
})
export class PrayersService extends FirestoreRepositoryService<Prayer> {


  constructor(
    auth: AngularFireAuth,
    firestore: AngularFirestore
  ) {
    super(auth, firestore, 'prayers');
  }
}
