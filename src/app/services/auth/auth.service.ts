import { Injectable } from '@angular/core';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private auth: AngularFireAuth,
    private googlePlus: GooglePlus,
    private toast: ToastController
  ) { }

  async signInWithEmail(email, password) {
    const promiseMethods = this.auth.auth.fetchSignInMethodsForEmail(email);
    promiseMethods.catch((error) => this.error(error));
    const methods = await promiseMethods;
    if (methods.some(m => m === 'password')) {
      const promiseSignIn = this.auth.auth.signInWithEmailAndPassword(email, password);
      promiseSignIn.catch((error) => this.error(error));
      return await promiseSignIn;
    } else {
      const promiseCreate = this.auth.auth.createUserWithEmailAndPassword(email, password);
      promiseCreate.catch((error) => this.error(error));
      return await promiseCreate;
    }
  }

  async linkWithEmail(user, email, password) {
    const cred = firebase.auth.EmailAuthProvider.credential(email, password);
    const promiseLink = user.linkWithCredential(cred);
    promiseLink.catch((error) => this.error(error));
    return await promiseLink;
  }

  async signInWithGoogle() {
    const googlePlusUser = await this.logInWithGooglePlus();
    const credential = firebase.auth.GoogleAuthProvider.credential(googlePlusUser.idToken);
    const promiseSignIn = this.auth.auth.signInWithCredential(credential);
    promiseSignIn.catch((error) => this.error(error));
    return await promiseSignIn;
  }

  async linkWithGoogle() {
    const googlePlusUser = await this.logInWithGooglePlus();
    const promiseLink = firebase.auth().currentUser.linkWithCredential(firebase.auth.GoogleAuthProvider.credential(googlePlusUser.idToken));
    promiseLink.catch((error) => this.error(error));
    return await promiseLink;
  }

  private async logInWithGooglePlus() {
    const promiseLogIn = this.googlePlus.login({
      webClientId: '482266103888-35161rjo8if60g8mb8bl5dj9ckh9a9qm.apps.googleusercontent.com',
      offline: 'true',
      scopes: 'profile email'
    });
    promiseLogIn.catch((error) => this.error(error));
    return await promiseLogIn;
  }

  private async error(error) {
    this.toast.create({ message: error.message || `Error: ${error}`, duration: 3000 }).then((toast) => toast.present());
  }

}
