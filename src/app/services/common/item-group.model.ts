import { ListItem } from "./list-item.model";

export class ItemGroup<T extends ListItem> {
    label: string;
    items: T[] = [];
}
