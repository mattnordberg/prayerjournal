export class PagedResults<T>{
    results: Array<T>;
    isLastPage: boolean;
}