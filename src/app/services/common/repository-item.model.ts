import { DocumentReference } from '@angular/fire/firestore';

export class RepositoryItem {
    id: string;
    userId: string;

    timestamp: firebase.firestore.Timestamp;
    date?: string;
    time?: string;
    
    doc?: DocumentReference;
}
