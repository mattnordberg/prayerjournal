import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, mergeMap, first } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { RepositoryItem } from './repository-item.model';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export abstract class FirestoreRepositoryService<T extends RepositoryItem> {

  constructor(
    private auth: AngularFireAuth,
    private firestore: AngularFirestore,
    private collection: string
  ) {

  }

  all(options?: any): Observable<T[]> {
    return this.auth.user.pipe(mergeMap(user => {
      if (!user) { return []; }
      return this.firestore.collection(this.collection, ref => {
        let query = ref.where('userId', '==', user.uid).orderBy('timestamp', 'desc');
        if (options) {
          Object.keys(options).filter(key => !!options[key]).forEach(key => {
            query = query.where(key, '==', options[key]);
          });
        }
        return query;
      }).valueChanges().pipe(map((a: T[]) => {
        return a.filter(o => o.id !== undefined)
          .map(o => {
            const date = moment(o.timestamp.seconds * 1000).format('MM/DD/YYYY');
            o.time = moment(o.timestamp.seconds * 1000).format('h:mm A');
            o.date = moment(date).isSameOrAfter(moment().format('MM/DD/YYYY'))
              ? 'Today'
              : moment(date).isSameOrAfter(moment().add(-1, 'days').format('MM/DD/YYYY'))
                ? 'Yesterday'
                : date;
            return o;
          });
      }));
    }));
  }

  promiseAll(options?: any): Promise<T[]> {
    return this.all(options).pipe(first()).toPromise();
  }

  one(id: string | firebase.firestore.FieldValue): Observable<T> {
    return this.auth.user.pipe(mergeMap(user => {
      if (!user) { return null; }
      return this.firestore
        .collection(this.collection, ref => ref.where('userId', '==', user.uid))
        .doc(id.toString())
        .valueChanges()
        .pipe(map(o => o as T));
    }));
  }

  promiseOne(id: string | firebase.firestore.FieldValue): Promise<T> {
    return this.one(id).pipe(first()).toPromise();
  }

  async create(item: Partial<T>): Promise<T> {
    const user = await this.auth.auth.currentUser;
    if (!user) { return null; }
    item.userId = user.uid;
    const added = await this.firestore.collection(this.collection, ref => ref.where('userId', '==', user.uid)).add(item);
    item.id = added.id;
    await this.firestore
      .collection(this.collection, ref => ref.where('userId', '==', user.uid))
      .doc(item.id)
      .update({ id: item.id });
    return item as T;
  }

  async update(id: string | firebase.firestore.FieldValue, data: Partial<T>): Promise<void> {
    const user = await this.auth.auth.currentUser;
    if (!user) { return null; }
    await this.firestore
      .collection(this.collection, ref => ref.where('userId', '==', user.uid))
      .doc(id.toString())
      .update(data);
  }

  async delete(id: string | firebase.firestore.FieldValue): Promise<void> {
    const user = await this.auth.auth.currentUser;
    if (!user) { return null; }
    await this.firestore
      .collection(this.collection, ref => ref.where('userId', '==', user.uid))
      .doc(id.toString())
      .delete();
  }

}
