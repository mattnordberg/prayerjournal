import { RepositoryItem } from "./repository-item.model";

export class ListItem extends RepositoryItem {
    title: string;
    text: string;

    preview?: string;
}