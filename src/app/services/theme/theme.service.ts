import { Injectable, Inject } from '@angular/core';

import { DOCUMENT } from '@angular/common';

import * as Color from 'color';

import { Storage } from '@ionic/storage';



@Injectable({

  providedIn: 'root'

})

export class ThemeService {

  themes = {

    autumn: {
      primary: '#B2675E',
      secondary: '#644536',
      tertiary: '#C2C094',
      light: '#D9C4AE',
      medium: '#C4A381',
      dark: '#A1866A'
    },
  
    neon: {
      primary: '#39BFBD',
      secondary: '#4CE0B3',
      tertiary: '#FF5E79',
      light: '#F4EDF2',
      medium: '#B682A5',
      dark: '#34162A'
    },
  
    driftwood: {
      primary: '#492C1D',
      secondary: '#6B7F82',
      tertiary: '#5B5750',
      light: '#C1D8F0',
      medium: '#8EB8E5',
      dark: '#4E657D'
    },
  
    mint: {
      primary: '#678D58',
      secondary: '#776D5A',
      tertiary: '#607466',
      light: '#AEDCC0',
      medium: '#607466',
      dark: '#343E3D'
    },
  
    night: {
      primary: '#8CBA80',
      secondary: '#FCFF6C',
      tertiary: '#FE5F55',
      medium: '#BCC2C7',
      dark: '#F7F7FF',
      light: '#495867'
    }
  
  };

  constructor(

    @Inject(DOCUMENT) private document: Document,

    private storage: Storage

  ) {

    storage.get('theme').then(cssText => {

      this.setGlobalCSS(cssText);

    });

  }



  // Override all global variables with a new theme

  setTheme(name) {

    const theme = this.themes[name];

    const cssText = CSSTextGenerator(theme);

    this.setGlobalCSS(cssText);

    this.storage.set('theme', cssText);

  }



  // Define a single CSS variable

  setVariable(name, value) {

    this.document.documentElement.style.setProperty(name, value);

  }



  private setGlobalCSS(css: string) {

    this.document.documentElement.style.cssText = css;

  }



  get storedTheme() {

    return this.storage.get('theme');

  }

}



const defaults = {

  primary: '#3880ff',

  secondary: '#0cd1e8',

  tertiary: '#7044ff',

  success: '#10dc60',

  warning: '#ffce00',

  danger: '#f04141',

  dark: '#222428',

  medium: '#989aa2',

  light: '#f4f5f8'

};



function CSSTextGenerator(colors) {

  colors = { ...defaults, ...colors };



  const {

    primary,

    secondary,

    tertiary,

    success,

    warning,

    danger,

    dark,

    medium,

    light

  } = colors;



  const shadeRatio = 0.1;

  const tintRatio = 0.1;



  return `

    --ion-color-base: ${light};

    --ion-color-contrast: ${dark};

    --ion-background-color: ${light};

    --ion-text-color: ${dark};

    --ion-toolbar-text-color: ${contrast(light, colors)};

    --ion-item-background-color: ${contrast(light, colors)};

    --ion-item-text-color: ${contrast(dark, colors)};


    --ion-color-primary: ${primary};

    --ion-color-primary-rgb: 56,128,255;

    --ion-color-primary-contrast: ${contrast(primary, colors)};

    --ion-color-primary-contrast-rgb: 255,255,255;

    --ion-color-primary-shade:  ${Color(primary).darken(shadeRatio)};

    --ion-color-primary-tint:  ${Color(primary).lighten(tintRatio)};



    --ion-color-secondary: ${secondary};

    --ion-color-secondary-rgb: 12,209,232;

    --ion-color-secondary-contrast: ${contrast(secondary, colors)};

    --ion-color-secondary-contrast-rgb: 255,255,255;

    --ion-color-secondary-shade:  ${Color(secondary).darken(shadeRatio)};

    --ion-color-secondary-tint: ${Color(secondary).lighten(tintRatio)};



    --ion-color-tertiary:  ${tertiary};

    --ion-color-tertiary-rgb: 112,68,255;

    --ion-color-tertiary-contrast: ${contrast(tertiary, colors)};

    --ion-color-tertiary-contrast-rgb: 255,255,255;

    --ion-color-tertiary-shade: ${Color(tertiary).darken(shadeRatio)};

    --ion-color-tertiary-tint:  ${Color(tertiary).lighten(tintRatio)};



    --ion-color-success: ${success};

    --ion-color-success-rgb: 16,220,96;

    --ion-color-success-contrast: ${contrast(success, colors)};

    --ion-color-success-contrast-rgb: 255,255,255;

    --ion-color-success-shade: ${Color(success).darken(shadeRatio)};

    --ion-color-success-tint: ${Color(success).lighten(tintRatio)};



    --ion-color-warning: ${warning};

    --ion-color-warning-rgb: 255,206,0;

    --ion-color-warning-contrast: ${contrast(warning, colors)};

    --ion-color-warning-contrast-rgb: 255,255,255;

    --ion-color-warning-shade: ${Color(warning).darken(shadeRatio)};

    --ion-color-warning-tint: ${Color(warning).lighten(tintRatio)};



    --ion-color-danger: ${danger};

    --ion-color-danger-rgb: 245,61,61;

    --ion-color-danger-contrast: ${contrast(danger, colors)};

    --ion-color-danger-contrast-rgb: 255,255,255;

    --ion-color-danger-shade: ${Color(danger).darken(shadeRatio)};

    --ion-color-danger-tint: ${Color(danger).lighten(tintRatio)};



    --ion-color-dark: ${dark};

    --ion-color-dark-rgb: 34,34,34;

    --ion-color-dark-contrast: ${contrast(dark, colors)};

    --ion-color-dark-contrast-rgb: 255,255,255;

    --ion-color-dark-shade: ${Color(dark).darken(shadeRatio)};

    --ion-color-dark-tint: ${Color(dark).lighten(tintRatio)};



    --ion-color-medium: ${medium};

    --ion-color-medium-rgb: 152,154,162;

    --ion-color-medium-contrast: ${contrast(medium, colors)};

    --ion-color-medium-contrast-rgb: 255,255,255;

    --ion-color-medium-shade: ${Color(medium).darken(shadeRatio)};

    --ion-color-medium-tint: ${Color(medium).lighten(tintRatio)};



    --ion-color-light: ${light};

    --ion-color-light-rgb: 244,244,244;

    --ion-color-light-contrast: $${contrast(light, colors)};

    --ion-color-light-contrast-rgb: 0,0,0;

    --ion-color-light-shade: ${Color(light).darken(shadeRatio)};

    --ion-color-light-tint: ${Color(light).lighten(tintRatio)};`;

}



function contrast(color, colors) {

  color = Color(color);

  return color.isDark() ? colors.light : colors.dark;

}