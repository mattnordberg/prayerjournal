import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Answer } from './answer.model';
import { map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { ItemGroup } from '../common/item-group.model';
import * as moment from 'moment';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirestoreRepositoryService } from '../common/firestore-repository.service';

@Injectable({
  providedIn: 'root'
})
export class AnswersService extends FirestoreRepositoryService<Answer> {

  constructor(
    auth: AngularFireAuth,
    firestore: AngularFirestore
  ) {
    super(auth, firestore, 'answers');
  }

}
