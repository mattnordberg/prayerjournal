import { ListItem } from '../common/list-item.model';

export class Answer extends ListItem {
    prayerId?: string | firebase.firestore.FieldValue;
    requestId?: string | firebase.firestore.FieldValue;
}
