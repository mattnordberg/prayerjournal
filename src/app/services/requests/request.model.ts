import { ListItem } from "../common/list-item.model";

export class Request extends ListItem {
    complete: boolean;
}