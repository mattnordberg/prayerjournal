import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Request } from './request.model';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { ItemGroup } from '../common/item-group.model';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { FirestoreRepositoryService } from '../common/firestore-repository.service';

@Injectable({
  providedIn: 'root'
})
export class RequestsService extends FirestoreRepositoryService<Request> {

  constructor(
    auth: AngularFireAuth,
    firestore: AngularFirestore
  ) {
    super(auth, firestore, 'requests');
  }

}
