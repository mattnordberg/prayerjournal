import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'auth', loadChildren: './pages/auth/auth.module#AuthModule' },
  { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsModule' },
  { path: 'start', loadChildren: './pages/start/start.module#StartModule' },
  { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsModule' },
  { path: 'prayers', loadChildren: './pages/prayers/prayers.module#PrayersModule' },
  { path: 'requests', loadChildren: './pages/requests/requests.module#RequestsModule' },
  { path: 'answers', loadChildren: './pages/answers/answers.module#AnswersModule' },
  { path: '', redirectTo: 'tabs/prayers', pathMatch: 'full' },
  {path: '__/auth/action', redirectTo: 'auth/email/verify', pathMatch: 'prefix'},
  {path: '__/auth/handler', redirectTo: 'auth/email/verify', pathMatch: 'prefix'}
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRouting { }
