import { Component } from '@angular/core';
import { IonItemSliding, NavController } from '@ionic/angular';
import { RequestsService } from 'src/app/services/requests/requests.service';
import { Request } from 'src/app/services/requests/request.model';
import { ItemGroup } from 'src/app/services/common/item-group.model';

@Component({
  selector: 'app-requests',
  templateUrl: 'requests.page.html',
  styleUrls: ['requests.page.scss']
})
export class RequestsPage {

  items: Request[] = [];
  ready: boolean;

  constructor(
    private navCtrl: NavController,
    private requests: RequestsService
  ) {
    this.requests.all().subscribe(items => {
      this.items = items;
      this.ready = true;
    });
  }

  add() {
    this.navCtrl.navigateForward('/tabs/requests/add');
  }

  edit(item: Request) {
    this.navCtrl.navigateForward(`/tabs/requests/${item.id}`);
  }

  complete(item: Request) {
    item.complete = true;
    this.requests.update(item.id, { complete: true });
  }

  async addPrayer(item: Request) {
    this.navCtrl.navigateForward('/prayers/add', { queryParams: { requestId: item.id } });
  }

  itemNotComplete(item: Request): boolean {
    return item && !item.complete;
  }

}
