import { Component } from '@angular/core';
import { Request } from 'src/app/services/requests/request.model';
import { RequestsService } from 'src/app/services/requests/requests.service';
import { AlertController, NavController, ActionSheetController } from '@ionic/angular';
import * as firebase from 'firebase';
import { ActivatedRoute } from '@angular/router';
import { ActionSheetButton } from '@ionic/core';

@Component({
  selector: 'app-request',
  templateUrl: 'request.page.html',
  styleUrls: ['request.page.scss']
})
export class RequestPage {

  hasFocus: any = {};
  isNew: boolean;
  item: Request;
  itemId: string;

  constructor(
    private actionCtrl: ActionSheetController,
    private activatedRoute: ActivatedRoute,
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private requestsService: RequestsService
  ) {

  }

  ionViewDidEnter() {
    this.activatedRoute.params.subscribe((params) => {
      this.itemId = params.id;
      if (!this.itemId || this.itemId === 'add') {
        this.isNew = true;
        this.requestsService
          .create({
            timestamp: firebase.firestore.Timestamp.now(),
            title: '',
            text: ''
          })
          .then((added) => {
            this.item = added;
          });
      } else {
        this.requestsService.one(this.itemId).subscribe((item) => {
          this.item = item;
        });
        this.isNew = false;
      }
    });
  }

  delete() {
    if (this.item && this.item.text) {
      this.alertCtrl.create({
        header: 'Delete Entry',
        message: 'Are you sure you want to delete this entry?',
        buttons: [
          { text: 'Cancel', cssClass: 'secondary' },
          {
            text: 'Delete',
            role: 'destructive',
            handler: () => {
              this.finishDelete();
            }
          }
        ]
      }).then((confirm) => confirm.present());
    } else {
      this.finishDelete();
    }
  }

  finishDelete() {
    this.requestsService.delete(this.item.id).then(() => {
      this.navCtrl.pop();
    });
  }

  save() {
    if (!this.item) {
      return;
    }
    this.requestsService.update(this.item.id, { text: this.item.text, title: this.item.title, complete: false });
  }

  showMenu() {
    const buttons: ActionSheetButton[] = [
      {
        text: 'Delete',
        role: 'destructive',
        handler: () => this.delete()
      },
      {
        text: 'Pray for this Request',
        handler: () => this.addPrayer()
      },
      {
        text: 'New Answer',
        handler: () => this.addAnswer()
      },
      {
        text: 'Linked Prayers',
        handler: () => this.listPrayers()
      },
      {
        text: 'Linked Answers',
        handler: () => this.listAnswers()
      },
      {
        text: 'Cancel',
        role: 'cancel',
      }
    ];
    if (!this.item.complete) {
      buttons.push({
        text: 'Mark Complete',
        handler: () => this.complete()
      });
    }
    this.actionCtrl.create({
      buttons: buttons
    }).then((sheet) => sheet.present());
  }

  addPrayer() {
    this.navCtrl.navigateForward(['/prayers/add'], { queryParams: { requestId: this.item.id } });
  }

  listPrayers() {
    this.navCtrl.navigateForward(['/prayers'], { queryParams: { requestId: this.item.id } });
  }

  addAnswer() {
    this.navCtrl.navigateForward(['/answers/add'], { queryParams: { requestId: this.item.id } });
  }

  listAnswers() {
    this.navCtrl.navigateForward(['/answers'], { queryParams: { requestId: this.item.id } });
  }

  complete() {
    this.requestsService.update(this.item.id, { complete: true }).then(() => {
      this.navCtrl.pop();
    });
  }

}
