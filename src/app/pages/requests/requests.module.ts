import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RequestsPage } from './requests.page';
import { RequestPage } from './request/request.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: RequestsPage },
      { path: ':id', component: RequestPage }
    ]),
    ComponentsModule
  ],
  declarations: [
    RequestsPage,
    RequestPage
  ]
})
export class RequestsModule {}
