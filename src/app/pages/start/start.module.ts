import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StartPage } from './start.page';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [
    StartPage
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild([
      { path: '', component: StartPage }
    ])
  ]
})
export class StartModule { }
