import { Component, Input } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-start',
  templateUrl: 'start.page.html',
  styleUrls: ['start.page.scss']
})
export class StartPage {

  constructor(
    private alert: AlertController,
    private auth: AngularFireAuth,
    private authService: AuthService,
    private navCtrl: NavController
  ) {
    this.auth.user.subscribe(user => {
      if (user) {
        this.navCtrl.navigateRoot('/tabs/prayers', { animationDirection: 'forward' });
      }
    });
  }

  signInWithGoogle() {
    this.authService.signInWithGoogle().then((user) => {
      if (user) {
        this.navCtrl.navigateRoot('/tabs/prayers', { animationDirection: 'forward' });
      }
    });
  }

  async signInAnonymously() {
    this.alert.create({
      header: 'Skip Sign-in',
      // tslint:disable-next-line: max-line-length
      message: 'Are you sure you want to skip signing in? Your prayer requests and journal entries will only be available on this device, and may be lost if you don\'t sign in later.',
      backdropDismiss: false,
      buttons: [
        { text: 'Cancel', cssClass: 'secondary' },
        {
          text: 'Skip sign-in', handler: () => {
            this.alert.create({
              header: 'Skip Sign-in',
              // tslint:disable-next-line: max-line-length
              message: ' You can choose to sign in later to sync your content across devices. Just go to Menu > Profile to sign in at any time.',
              backdropDismiss: false,
              buttons: [
                {
                  text: 'Got it', handler: () => {
                    this.auth.auth.signInAnonymously();
                  }
                }
              ]
            }).then((alert) => alert.present());
          }
        }
      ]
    }).then((alert) => alert.present());
  }

}
