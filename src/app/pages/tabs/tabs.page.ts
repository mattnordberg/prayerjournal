import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  private lastTab: string;

  constructor(
    private router: Router
  ) {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd && e.url === '/tabs') {
        this.router.navigateByUrl(`/tabs/${this.lastTab || 'prayers'}`);
      }
    });
  }

  // workaround for https://github.com/ionic-team/ionic/issues/17278
  tabChanged(tab: string) {
    this.lastTab = tab;
  }

}
