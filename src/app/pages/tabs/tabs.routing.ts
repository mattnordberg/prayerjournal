import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'requests',
        children: [
          {
            path: '',
            loadChildren: '../requests/requests.module#RequestsModule'
          }
        ]
      },
      {
        path: 'prayers',
        children: [
          {
            path: '',
            loadChildren: '../prayers/prayers.module#PrayersModule'
          }
        ]
      },
      {
        path: 'answers',
        children: [
          {
            path: '',
            loadChildren: '../answers/answers.module#AnswersModule'
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRouting {}
