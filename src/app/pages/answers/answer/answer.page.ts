import { Component } from '@angular/core';
import { Answer } from 'src/app/services/answers/answer.model';
import { AnswersService } from 'src/app/services/answers/answers.service';
import { AlertController, NavController, ActionSheetController, PopoverController, ToastController } from '@ionic/angular';
import * as firebase from 'firebase';
import { RequestsService } from 'src/app/services/requests/requests.service';
import { ActivatedRoute } from '@angular/router';
import { ActionSheetButton } from '@ionic/core';
import { GroupedListPopover } from 'src/app/popovers/grouped-list/grouped-list.popover';
import { PrayersService } from 'src/app/services/prayers/prayers.service';

@Component({
  selector: 'app-answer',
  templateUrl: 'answer.page.html',
  styleUrls: ['answer.page.scss']
})
export class AnswerPage {

  isNew: boolean;
  item: Answer;
  itemId: string;
  requestCompleted: boolean;
  prayerId: string;
  requestId: string;

  constructor(
    private actionSheetCtrl: ActionSheetController,
    private activatedRoute: ActivatedRoute,
    private alertCtrl: AlertController,
    private answersService: AnswersService,
    private navCtrl: NavController,
    private popoverCtrl: PopoverController,
    private prayersService: PrayersService,
    private requestsService: RequestsService,
    private toastCtrl: ToastController
  ) {

  }

  async ionViewDidEnter() {
    this.activatedRoute.queryParams.subscribe((queryParams) => {
      this.prayerId = queryParams.prayerId;
      this.requestId = queryParams.requestId;
      this.activatedRoute.params.subscribe((params) => {
        this.itemId = params.id;
        if (!this.itemId || this.itemId === 'add') {
          this.isNew = true;
          this.answersService.create({
            prayerId: this.prayerId || null,
            requestId: this.requestId || null,
            timestamp: firebase.firestore.Timestamp.now(),
            text: ''
          }).then((added) => {
            this.item = added;
          });
        } else {
          this.answersService.one(this.itemId).subscribe((item) => {
            this.item = item;
          });
          this.isNew = false;
        }
      });
    });
  }

  async delete(): Promise<boolean> {
    if (this.item && this.item.text) {
      const confirm = await this.alertCtrl.create({
        header: 'Delete Entry',
        message: 'Are you sure you want to delete this entry?',
        buttons: [
          { text: 'Cancel', cssClass: 'secondary' },
          {
            text: 'Delete', handler: () => {
              this.finishDelete();
            }
          }
        ]
      });
      confirm.present();
    } else {
      this.finishDelete();
    }
    return true;
  }

  finishDelete() {
    this.answersService.delete(this.item.id).then(() => {
      this.navCtrl.pop();
    });
  }

  save() {
    if (!this.item) {
      return;
    }
    this.answersService.update(this.item.id, { text: this.item.text }).then(() => {
      if (this.item.text && this.item.prayerId && !this.requestCompleted) {
        this.requestsService.update(this.item.prayerId, { complete: true });
      }
    });
  }

  goToPrayer() {
    this.navCtrl.navigateForward(`/prayers/${this.item.prayerId}`);
  }

  goToRequest() {
    this.navCtrl.navigateForward(`/requests/${this.item.requestId}`);
  }

  async linkPrayer() {
    const popover = await this.popoverCtrl.create({
      component: GroupedListPopover,
      cssClass: 'centered',
      componentProps: {
        title: 'Select Prayer',
        items: await this.prayersService.promiseAll()
      }
    });
    popover.onDidDismiss().then((event) => {
      if (event && event.data && event.data.id) {
        this.item.prayerId = event.data.id;
        this.answersService.update(this.item.id, { prayerId: this.item.prayerId }).then(() => {
          this.toast('Prayer has been linked');
        });
      }
    });
    popover.present();
    return true;
  }

  unlinkPrayer() {
    this.answersService.update(this.item.id, { prayerId: firebase.firestore.FieldValue.delete() }).then(() => {
      this.item.prayerId = null;
      this.toast('Prayer has been unlinked');
    });
  }

  async linkRequest() {
    const popover = await this.popoverCtrl.create({
      component: GroupedListPopover,
      cssClass: 'centered',
      componentProps: {
        title: 'Select Request',
        items: await this.requestsService.promiseAll()
      }
    });
    popover.onDidDismiss().then((event) => {
      if (event && event.data && event.data.id) {
        this.item.requestId = event.data.id;
        this.answersService.update(this.item.id, { requestId: this.item.requestId }).then(() => {
          this.toast('Request has been linked');
        });
      }
    });
    popover.present();
    return true;
  }

  unlinkRequest() {
    this.answersService.update(this.item.id, { requestId: firebase.firestore.FieldValue.delete() }).then(() => {
      this.item.requestId = null;
      this.toast('Request has been unlinked');
    });
  }

  showMenu() {
    const buttons: ActionSheetButton[] = [
      {
        text: 'Cancel',
        role: 'cancel'
      }
    ];
    if (!this.isNew) {
      buttons.push({
        text: 'Delete',
        role: 'destructive',
        handler: () => this.delete()
      });
    }
    if (!this.prayerId && this.item.prayerId) {
      buttons.push({
        text: 'See Prayer',
        handler: () => this.goToPrayer()
      });
      buttons.push({
        text: 'Unlink Prayer',
        handler: () => this.unlinkPrayer()
      });
    }
    if (!this.requestId && this.item.requestId) {
      buttons.push({
        text: 'See Request',
        handler: () => this.goToRequest()
      });
      buttons.push({
        text: 'Unlink Request',
        handler: () => this.unlinkRequest()
      });
    }
    if (!this.prayerId && !this.item.prayerId) {
      buttons.push({
        text: 'Link a Prayer',
        handler: () => this.linkPrayer()
      });
    }
    if (!this.requestId && !this.prayerId && !this.item.prayerId && !this.item.requestId) {
      buttons.push({
        text: 'Link a Request',
        handler: () => this.linkRequest()
      });
    }
    this.actionSheetCtrl.create({
      buttons: buttons
    }).then((sheet) => sheet.present());
  }

  toast(message: string) {
    this.toastCtrl.create({
      message: message,
      duration: 3000
    }).then((toast) => toast.present());
  }

}
