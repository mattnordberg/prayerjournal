import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AnswersPage } from './answers.page';
import { AnswerPage } from './answer/answer.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { PopoversModule } from 'src/app/popovers/popovers.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: AnswersPage },
      { path: ':id', component: AnswerPage }
    ]),
    ComponentsModule,
    PopoversModule
  ],
  declarations: [
    AnswersPage,
    AnswerPage
  ]
})
export class AnswersModule { }
