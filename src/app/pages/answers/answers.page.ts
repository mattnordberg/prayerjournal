import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AnswersService } from 'src/app/services/answers/answers.service';
import { ItemGroup } from 'src/app/services/common/item-group.model';
import { Answer } from 'src/app/services/answers/answer.model';

@Component({
  selector: 'app-answers',
  templateUrl: 'answers.page.html',
  styleUrls: ['answers.page.scss']
})
export class AnswersPage {

  items: Answer[] = [];
  ready: boolean;
  prayerId: string;
  requestId: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private answersService: AnswersService,
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.prayerId = params.prayerId;
      this.requestId = params.requestId;
      this.answersService.all({ prayerId: this.prayerId, requestId: this.requestId }).subscribe(items => {
        this.items = items;
        this.ready = true;
      });
    });
  }

  add() {
    this.navCtrl.navigateForward(['/answers/add'], { queryParams: { prayerId: this.prayerId } });
  }

  edit(item?: Answer) {
    this.navCtrl.navigateForward([`/answers/${item.id}`], { queryParams: { prayerId: this.prayerId } });
  }

}
