import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { ThemeService } from 'src/app/services/theme/theme.service';

@Component({
  selector: 'app-settings',
  templateUrl: 'settings.page.html',
  styleUrls: ['settings.page.scss']
})
export class SettingsPage {

  settings: any;
  theme: string;
  themes: string[] = Object.keys(this.themeService.themes);
  user: User;

  constructor(
    private auth: AngularFireAuth,
    private store: AngularFirestore,
    private themeService: ThemeService
  ) {
    auth.user.subscribe(user => {
      this.user = user;
      if (this.user) {
        store.collection('user-settings').doc(user.uid).valueChanges().subscribe((settings: any) => {
          this.settings = settings;
          if (!this.settings) {
            store.collection('user-settings').doc(user.uid).set({});
          }
        });
      }
    });


  }

  toggleOfflineMode() {
    if (this.settings && this.settings.offlineMode) {
      this.store.firestore.disableNetwork();
      this.settings.wifiOnly = false;
    } else if (this.settings && !this.settings.offlineMode) {
      this.store.firestore.enableNetwork();
      this.save();
    }
  }

  save(partial?: any) {
    this.store.collection('user-settings').doc(this.user.uid).update(partial || this.settings);
  }

  changeTheme(name) {
    this.themeService.setTheme(name);
    this.save({ theme: name });
  }

  getButtonColor(name) {
    return this.themeService.themes[name] && this.themeService.themes[name].medium || '#FFFFFF';
  }

}
