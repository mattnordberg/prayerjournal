import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailSignInPage } from './email-sign-in.page';

describe('EmailSignInPage', () => {
  let component: EmailSignInPage;
  let fixture: ComponentFixture<EmailSignInPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmailSignInPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailSignInPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
