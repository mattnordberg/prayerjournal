import { Component, Input } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-email-sign-in',
  templateUrl: 'email-sign-in.page.html',
  styleUrls: ['email-sign-in.page.scss']
})
export class EmailSignInPage {

  email: string;
  password: string;
  ready: boolean;
  user: firebase.User;
  submitting: boolean;

  constructor(
    private auth: AngularFireAuth,
    private authService: AuthService,
    private navCtrl: NavController,
    private toast: ToastController
  ) {
    this.auth.user.subscribe((user) => {
      this.user = user;
      this.ready = true;
    });
  }

  submit() {
    this.submitting = true;
    if (this.user && this.user.isAnonymous) {
      this.authService.linkWithEmail(this.user, this.email, this.password).then(() => this.finish(), () => this.submitting = false);
    } else {
      this.authService.signInWithEmail(this.email, this.password).then(() => this.finish(), () => this.submitting = false);
    }
  }

  finish() {
    this.navCtrl.navigateRoot('/', { animationDirection: 'forward' });
    this.toast.create({
      message: `You are now signed is as ${this.email}`,
      duration: 3000
    }).then((toast) => toast.present());
    this.submitting = false;
  }



}
