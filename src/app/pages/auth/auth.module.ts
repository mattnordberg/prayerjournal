import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthPage } from './auth.page';
import { EmailSignInPage } from './email/email-sign-in.page';
import { IonicModule } from '@ionic/angular';
import { ProfilePage } from './profile/profile.page';
import { FormsModule } from '@angular/forms';
import { IonicStorageModule } from '@ionic/storage';
import { NgCircleProgressModule } from 'ng-circle-progress';

@NgModule({
  declarations: [
    AuthPage,
    EmailSignInPage,
    ProfilePage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicStorageModule,
    RouterModule.forChild([
      { path: '', component: AuthPage },
      { path: 'email-sign-in', component: EmailSignInPage },
      { path: 'profile', component: ProfilePage }
    ]),
    NgCircleProgressModule
  ]
})
export class AuthModule { }