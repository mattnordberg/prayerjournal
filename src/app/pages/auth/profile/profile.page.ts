import { Component } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.page.html',
  styleUrls: ['profile.page.scss']
})
export class ProfilePage {

  user: User;
  profile: any;
  signIn: boolean;

  uploading: boolean;
  uploadPercentage: number;

  get progressRadius() {
    return document.documentElement.clientWidth;
  }

  constructor(
    private alert: AlertController,
    private auth: AngularFireAuth,
    private authService: AuthService,
    private storage: AngularFireStorage,
    private store: AngularFirestore,
    private toast: ToastController
  ) {
    this.getUser();
  }

  changeImage() {
    document.getElementById('profileImage').click();
  }

  setImage(fileInput: any) {
    this.uploading = true;
    this.uploadPercentage = 0;
    if (fileInput.target.files && fileInput.target.files[0]) {
      const upload = this.storage.upload(`/users/${this.user.uid}/profile`, fileInput.target.files[0]);
      upload.percentageChanges().subscribe((percentage) => {
        this.uploadPercentage = percentage;
      });
      upload.then((snapshot) => {
        snapshot.ref.getDownloadURL().then(downloadUrl => {
          this.updateProfile({
            photoURL: downloadUrl
          }).catch((error) => this.error(error));
        });
        this.uploading = false;
      }).catch((error) => {
        this.uploading = false;
        this.error(error);
      });
    }
  }

  getUser() {
    this.auth.user.subscribe(user => {
      this.user = user;
      if (!this.user) {
        return;
      }
      const profileDoc = this.store.collection('user-profiles').doc(user.uid);
      profileDoc.valueChanges().subscribe((profile: any) => {
        this.profile = profile;
        if (!this.profile) {
          profileDoc.set({
            displayName: user.displayName,
            photoURL: user.photoURL,
            email: user.email,
            phoneNumber: user.phoneNumber
          });
        } else {
          profile.email = profile.email || user.email;
          profile.displayName = profile.displayName || user.displayName;
          profile.photoURL = profile.photoURL || user.photoURL;
          this.updateProfile();
        }
      });
    });
  }

  async updateProfile(partial?: any) {
    return this.store.collection('user-profiles').doc(this.user.uid).update(partial || this.profile);
  }

  async signOut() {
    const alert = await this.alert.create({
      header: 'Sign Out',
      message: `Are you sure you want to sign out?${this.user && this.user.isAnonymous ? ' Any prayer requests and journal entries will be lost.' : ''}`,
      buttons: [
        { text: 'Cancel', cssClass: 'secondary' },
        {
          text: 'Sign out', handler: () => {
            this.auth.auth.signOut();
          }
        }
      ]
    });
    alert.present();
  }

  linkWithGoogle() {
    this.authService.linkWithGoogle().then(() => {
      this.getUser();
    }, (error) => {
      this.error(error);
    });
  }

  error(error) {
    this.toast.create({ message: error.message, duration: 3000 }).then((toast) => toast.present());
  }
}
