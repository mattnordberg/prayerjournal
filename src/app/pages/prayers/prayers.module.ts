import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PrayersPage } from './prayers.page';
import { PrayerPage } from './prayer/prayer.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: PrayersPage },
      { path: ':id', component: PrayerPage }
    ]),
    ComponentsModule
  ],
  declarations: [
    PrayersPage,
    PrayerPage
  ]
})
export class PrayersModule { }
