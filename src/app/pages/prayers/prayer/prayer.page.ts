import { Component, Input } from '@angular/core';
import { Prayer } from 'src/app/services/prayers/prayer.model';
import { PrayersService } from 'src/app/services/prayers/prayers.service';
import * as moment from 'moment';
import { NavParams, ModalController, AlertController, NavController, ActionSheetController, PopoverController, ToastController } from '@ionic/angular';
import * as firebase from 'firebase';
import { RequestsService } from 'src/app/services/requests/requests.service';
import { ActivatedRoute } from '@angular/router';
import { ActionSheetButton } from '@ionic/core';
import { GroupedListPopover } from 'src/app/popovers/grouped-list/grouped-list.popover';

@Component({
  selector: 'app-prayer',
  templateUrl: 'prayer.page.html',
  styleUrls: ['prayer.page.scss']
})
export class PrayerPage {

  hasFocus: any = {};
  isNew: boolean;
  item: Prayer;
  itemId: string;
  requestCompleted: boolean;
  requestId: string;

  constructor(
    private actionSheetCtrl: ActionSheetController,
    private activatedRoute: ActivatedRoute,
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private popoverCtrl: PopoverController,
    private prayersService: PrayersService,
    private requestsService: RequestsService,
    private toastCtrl: ToastController
  ) {

  }

  async ionViewDidEnter() {
    this.activatedRoute.queryParams.subscribe((queryParams) => {
      this.requestId = queryParams.requestId;
      this.activatedRoute.params.subscribe((params) => {
        this.itemId = params.id;
        if (!this.itemId || this.itemId === 'add') {
          this.isNew = true;
          this.prayersService
            .create({
              requestId: this.requestId || null,
              timestamp: firebase.firestore.Timestamp.now(),
              text: ''
            })
            .then((added) => {
              this.item = added;
            });
        } else {
          this.prayersService.one(this.itemId).subscribe((item) => {
            this.item = item;
          });
          this.isNew = false;
        }
      });
    });
  }

  async delete(): Promise<boolean> {
    if (this.item && this.item.text) {
      const confirm = await this.alertCtrl.create({
        header: 'Delete Entry',
        message: 'Are you sure you want to delete this entry?',
        buttons: [
          { text: 'Cancel', cssClass: 'secondary' },
          {
            text: 'Delete', handler: () => {
              this.finishDelete();
            }
          }
        ]
      });
      confirm.present();
    } else {
      this.finishDelete();
    }
    return true;
  }

  finishDelete() {
    this.prayersService.delete(this.item.id).then(() => {
      this.navCtrl.pop();
    });
  }

  save() {
    if (!this.item) {
      return;
    }
    this.prayersService.update(this.item.id, { text: this.item.text }).then(() => {
      if (this.item.text && this.item.requestId && !this.requestCompleted) {
        this.requestsService.update(this.item.requestId, { complete: true });
      }
    });
  }

  goToRequest() {
    this.navCtrl.navigateForward(`/requests/${this.item.requestId}`);
  }

  addAnswer() {
    this.navCtrl.navigateForward(['/answers/add'], { queryParams: { prayerId: this.item.id } });
  }

  listAnswers() {
    this.navCtrl.navigateForward(['/answers'], { queryParams: { prayerId: this.item.id } });
  }

  showMenu() {
    const buttons: ActionSheetButton[] = [
      {
        text: 'Cancel',
        role: 'cancel',
      }
    ];
    if (!this.isNew) {
      buttons.push({
        text: 'Delete',
        role: 'destructive',
        handler: () => this.delete()
      });
    }

    if (!this.requestId && !this.item.requestId) {
      buttons.push({
        text: 'Link Request',
        handler: () => this.linkRequest()
      });
    }
    buttons.push({
      text: 'New Answer',
      handler: () => this.addAnswer()
    });
    if (!this.requestId && this.item.requestId) {
      buttons.push({
        text: 'Linked Request',
        handler: () => this.goToRequest()
      });
    }
    buttons.push({
      text: 'Linked Answers',
      handler: () => this.listAnswers()
    });
    this.actionSheetCtrl.create({
      buttons: buttons
    }).then((sheet) => sheet.present());
  }

  async linkRequest() {
    const popover = await this.popoverCtrl.create({
      component: GroupedListPopover,
      cssClass: 'centered',
      componentProps: {
        title: 'Select Request',
        items: await this.requestsService.promiseAll()
      }
    });
    popover.onDidDismiss().then((event) => {
      if (event && event.data && event.data.id) {
        this.item.requestId = event.data.id;
        this.prayersService.update(this.item.id, { requestId: this.item.requestId }).then(() => {
          this.toast('Request has been linked');
        });
      }
    });
    popover.present();
    return true;
  }

  unlinkRequest() {
    this.prayersService.update(this.item.id, { requestId: firebase.firestore.FieldValue.delete() }).then(() => {
      this.item.requestId = null;
      this.toast('Request has been unlinked');
    });
  }

  toast(message: string) {
    this.toastCtrl.create({
      message: message,
      duration: 3000
    }).then((toast) => toast.present());
  }

}
