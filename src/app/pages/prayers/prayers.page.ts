import { Component, Input } from '@angular/core';
import { Prayer } from 'src/app/services/prayers/prayer.model';
import { PrayersService } from 'src/app/services/prayers/prayers.service';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { ItemGroup } from 'src/app/services/common/item-group.model';

@Component({
  selector: 'app-prayers',
  templateUrl: 'prayers.page.html',
  styleUrls: ['prayers.page.scss']
})
export class PrayersPage {

  items: Prayer[] = [];
  ready: boolean;
  requestId: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private prayersService: PrayersService,
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.requestId = params.requestId;
      this.prayersService.all({ requestId: this.requestId }).subscribe(items => {
        this.items = items;
        this.ready = true;
      });
    });
  }

  add() {
    this.navCtrl.navigateForward(['/prayers/add'], { queryParams: { requestId: this.requestId } });
  }

  edit(item?: Prayer) {
    this.navCtrl.navigateForward([`/prayers/${item.id}`], { queryParams: { requestId: this.requestId } });
  }

}
